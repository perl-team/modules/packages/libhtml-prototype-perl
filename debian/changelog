libhtml-prototype-perl (1.48-6) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * Change bugtracker URL(s) to HTTPS.
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Tim Retout from Uploaders. Thanks for your work!

  [ Alex Muntada ]
  * Remove inactive pkg-perl members from Uploaders.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from deprecated 8 to 12.
  * Set debhelper-compat version in Build-Depends.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 14 Jun 2022 22:44:26 +0100

libhtml-prototype-perl (1.48-5.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Tue, 05 Jan 2021 01:02:42 +0100

libhtml-prototype-perl (1.48-5) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.
  * Add explicit build dependency on libmodule-build-perl.

 -- gregor herrmann <gregoa@debian.org>  Sun, 07 Jun 2015 15:20:28 +0200

libhtml-prototype-perl (1.48-4) unstable; urgency=low

  * Team upload.

  [ Tim Retout ]
  * Email change: Tim Retout -> diocles@debian.org

  [ Ansgar Burchardt ]
  * Email change: Ansgar Burchardt -> ansgar@debian.org
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * debian/watch: remove obsolete comment.
  * Add a patch to fix POD encoding. (Closes: #709828)
  * Add a patch to fix a spelling mistake in the POD.
  * Switch to "3.0 (quilt)" source format.
  * Set Standards-Version to 3.9.4 (no changes).
  * Bump debhelper compatibility level to 8.
  * debian/copyright: switch formatting to Copyright-Format 1.0.

 -- gregor herrmann <gregoa@debian.org>  Sun, 26 May 2013 17:38:43 +0200

libhtml-prototype-perl (1.48-3) unstable; urgency=high

  * Set urgency to 'high' for security bug fix.
  * debian/control:
    + Add self to Uploaders.
    + Bump Standards-Version to 3.8.3.
    + Build-Depend on debhelper (>= 7.0.8) and quilt (>= 0.46-7).
    + Add Build-Depends-Indep and Depends on libjs-prototype and
      libjs-scriptaculous.
  * debian/rules: Add --with quilt.
  * debian/README.source: New standard quilt README.source.
  * debian/copyright: Add self to debian/* stanza.
  * debian/patches/use-system-prototype: New patch to make use of the
    prototype library provided by Debian, rather than the embedded copy.
    Addresses CVE-2007-2383 and CVE-2008-7720. (Closes: #558977)
  * debian/patches/use-system-scriptaculous-controls,
    debian/patches/use-system-scriptaculous-dragdrop,
    debian/patches/use-system-scriptaculous-effects: New patches to make
    use of the Debian libjs-scriptaculous library. (Closes: #538920)

 -- Tim Retout <tim@retout.co.uk>  Wed, 02 Dec 2009 10:59:47 +0000

libhtml-prototype-perl (1.48-2) unstable; urgency=low

  [ Ryan Niebur ]
  * Take over for the Debian Perl Group
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza);
    ${misc:Depends} to Depends: field. Changed: Maintainer set to Debian
    Perl Group <pkg-perl-maintainers@lists.alioth.debian.org> (was:
    Debian Catalyst Maintainers <pkg-catalyst-
    maintainers@lists.alioth.debian.org>); Debian Catalyst Maintainers
    <pkg-catalyst-maintainers@lists.alioth.debian.org> moved to
    Uploaders.
  * debian/watch: use dist-based URL.
  * Remove Florian Ragwitz from Uploaders (Closes: #523382)

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Ansgar Burchardt ]
  * Refresh rules for debhelper 7.
  * Convert debian/copyright to proposed machine-readable format.
  * Add myself to Uploaders.
  * Bump Standards-Version to 3.8.2.

  [ gregor herrmann ]
  * debian/copyright: add third-party copyright holder.
  * debian/control:
    - remove build dependency on libmodule-build-perl, debhelper prefers EUMM
    - make short description a noun phrase

 -- Ansgar Burchardt <ansgar@43-1.org>  Mon, 20 Jul 2009 14:49:25 +0200

libhtml-prototype-perl (1.48-1) unstable; urgency=low

  * New upstream release

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Mon, 25 Sep 2006 12:16:14 +0200

libhtml-prototype-perl (1.47-1) unstable; urgency=low

  * New upstream release
  * debian/compat: increased to 5

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Fri, 14 Jul 2006 12:10:29 +0200

libhtml-prototype-perl (1.45-1) unstable; urgency=low

  * New upstream release
  * debian/control:
   - Build-Depends: created and moved debhelper here
   - Standards-Version: increased to 3.7.2 without additional changes

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Tue, 30 May 2006 14:19:32 +0200

libhtml-prototype-perl (1.43-1) unstable; urgency=low

  * New upstream release

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Wed,  8 Feb 2006 23:16:16 +0100

libhtml-prototype-perl (1.41-1) unstable; urgency=low

  * New upstream release

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Thu, 19 Jan 2006 11:31:36 +0100

libhtml-prototype-perl (1.40-1) unstable; urgency=low

  * New upstream release
  * debian/watch - mirror changed

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Thu,  5 Jan 2006 10:28:26 +0100

libhtml-prototype-perl (1.36-1) unstable; urgency=low

  * New upstream release

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Fri,  2 Dec 2005 19:10:10 +0100

libhtml-prototype-perl (1.35-4) unstable; urgency=low

  * Minor fixes in debian directory (not for uploading)

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Tue, 15 Nov 2005 10:50:47 +0100

libhtml-prototype-perl (1.35-3) unstable; urgency=low

  * debian/watch added

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Tue, 15 Nov 2005 10:48:09 +0100

libhtml-prototype-perl (1.35-2) unstable; urgency=low

  * Moved perl build-dep-indep to build-dep. It's used in the clean target.
  * Added libmodule-build-perl to build-dep.
  * Added libhtml-tree-perl to build-dep-indep and Depends (Closes: #328936).

 -- Florian Ragwitz <rafl@debianforum.de>  Sun, 18 Sep 2005 15:40:58 +0200

libhtml-prototype-perl (1.35-1) unstable; urgency=low

  [ Krzysztof Krzyzaniak (eloy) ]
  * New upstream release

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Thu, 15 Sep 2005 13:23:07 +0200

libhtml-prototype-perl (1.34-1) unstable; urgency=low

  [ Krzysztof Krzyzaniak (eloy) ]
  * New upstream release
  * ITP Bug closing (closes: #327890)

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Wed, 14 Sep 2005 16:04:29 +0200

libhtml-prototype-perl (1.33-1) unstable; urgency=low

  [ Krzysztof Krzyzaniak (eloy) ]
  * New upstream release

  [ Florian Ragwitz ]
  * Added me to Uploaders.
  * Moved debhelper Build-Dep-Indep to Build-Depends because we use it in the
    clean target.
  * Removed unneeded debhelper calls in debian/rules
  * Don't install the README anymore. Its information is redundant.

 -- Florian Ragwitz <rafl@debianforum.de>  Thu,  8 Sep 2005 17:31:14 +0200

libhtml-prototype-perl (1.20-1) unstable; urgency=low

  * Initial Release.

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Tue,  5 Jul 2005 10:40:24 +0200
